# JAVA Spring Boot - com Actuator: Health Check, e Metrics para monitora

## O que vc precisa par rodar a aplicaçāo local

- openJDK11
- maven
- Docker

## Passos para a configuração

**1. Clone da aplicaçāo**

```bash
git clone 
```

**2. Build da aplicaçāo maven**

```bash
mvn clean package
```
**3. Roda a aplicaçāo**
```bash
mvn package
java -jar target/actuator-demo-0.0.1-SNAPSHOT.jar
```

Alternatively, you can run the app directly without packaging like this -

```bash
mvn spring-boot:run
```

**3. Docker built das image**

Instalaçāo do prometheus para metric em - Docker 

na pasta  main/resources o aqui dockerfile para contrçāo da image

```bash
cd proprometheus
docker build -t my-prometheus .
```

```bash
cd grafana
docker build -t my-grafana .
```

**4. Criaçāo de Container**

```bash
docker run -d --name=prometheus -p 9090:9090 my-prometheus
```

```bash
docker run -d --name=grafana -p 3000:3000 my-grafana 
```


The app will start running at <http://localhost:8080>.

## Explorar terminais do Endpoints


Todos actuator endpoints estará disponível em <http://localhost:8080/actuator>.


Alguns dos actuator endpoints são protegidos com a autenticação HTTP Basic da Spring Security. Você pode usar o nome de usuário `actuator` e senha `actuator` para autenticação básica de http.


