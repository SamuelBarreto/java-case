FROM openjdk

WORKDIR /HOME

COPY target/actuator-demo-0.0.1-SNAPSHOT.jar /HOME
EXPOSE 8080

CMD ["java", "-jar", "actuator-demo-0.0.1-SNAPSHOT.jar"]
